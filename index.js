const fs = require('fs');
const excelToJson = require('convert-excel-to-json');
const chalk = require('chalk');

const error = (data) => {
	console.log(chalk.bold.red(data));
};
const warning = chalk.keyword('orange');
const info = (data) => {
	console.log(chalk.keyword('cyan')(data));
};

const csv2json = (data) => {
	const arr = [];
	const lines = data.split('\n');
	const headerRow = lines.shift();
	let rowSplit = headerRow.split(',');

	const propHolder = {};
	rowSplit.forEach((item, index) => {
		if (item !== '') {
			propHolder[index] = item;
		}
	});

	return lines.map((item, index) => {
		let rowSplit = item.split(',');
		const obj = {};

		rowSplit.forEach((item, index) => {
			if (item !== '') {
				obj[propHolder[index]] = item;
			}
		});
		return obj;
	});
};

const textToCamel = (data) => {
	// console.log(data);
	data = data.replace(/[\(\)\#]/g, '');
	data = data.replace(/\✓/g, 'tick');
	let split = data.split(' ');
	split = split.map((item, index) => {
		item = item.toLowerCase();
		if (index > 0) {
			let str = '';
			for (let i = 0; i < item.length; i++) {
				str += i === 0 ? item[i].toUpperCase() : item[i];
			}
			item = str;
		}
		return item;
	});
	return split.join('');
};

const writeToModuleJsFile = (data, fileName) => {
	let str = `module.exports =  ${JSON.stringify(data, null, 3)}`;
	str = str.replace(/"([\w\d]*)":/g, '$1:');
	fs.writeFileSync(fileName, str);
};

const japWordWrapper = (input) => {
	return (
		'<span>' +
		input
			.split(/\n/)
			.map((i) => i.split('').join('&zwnj;'))
			.join('</span><span>') +
		'</span>'
	);
};

const process = (file, outFolder = '.') => {
	info(`reading file: ${file}`);
	info(`output will be in the folder: ${outFolder}`);
	const result = excelToJson({
		sourceFile: file,
	});
	const list = result['WEB_APP_EMAIL'];

	if (outFolder !== '.') {
		if (!fs.existsSync(outFolder)) {
			fs.mkdirSync(outFolder);
		}
	}
	const eng = {
		locale: 'en',
		subDomain: 'en',
	};
	const jap = {
		locale: 'ja',
		ubDomain: 'jp',
	};
	const varCounter = {};
	list.shift(); // remove first row;
	list.forEach((item) => {
		if (item.B) {
			const key = textToCamel(item.B);
			let counter = varCounter[key] || 0;
			eng[`${key}${counter === 0 ? '' : counter}`] = item.C;
			jap[`${key}${counter === 0 ? '' : counter}`] = japWordWrapper(item.D);
			counter += 1;
			varCounter[key] = counter;
		}
	});
	info(`creating files`);
	writeToModuleJsFile(eng, `${outFolder}/eng.js`);
	writeToModuleJsFile(jap, `${outFolder}/jap.js`);
};

module.exports = process;
