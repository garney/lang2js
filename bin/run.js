#!/usr/bin/env node

const chalk = require('chalk');

const error = chalk.bold.red;
const warning = chalk.keyword('orange');

// Delete the 0 and 1 argument (node and script.js)
var args = process.argv.splice(process.execArgv.length + 2);

var file = args[0];
var dir = args[1];
if (!file) {
	console.log(error('You need to specify a file'));
	return;
}

var myLibrary = require('../dist/lang2js.js');

myLibrary(file, dir);
